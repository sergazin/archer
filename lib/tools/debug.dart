import 'dart:developer' as developer;
import 'dart:convert';
import 'package:stack_trace/stack_trace.dart';
//import 'package:flutter_dotenv/flutter_dotenv.dart';

var devMode = true;

const debugEncoder = JsonEncoder.withIndent("  ");

Map<String, String> debugColors = {
  "red": "\x1b[31m",
  "green": "\x1b[32m",
  "yellow": "\x1b[33m",
  "blue": "\x1b[34m",
  "magenta": "\x1b[35m",
  "cyan": "\x1b[36m",
  "white": "\x1b[97m",
  "end": "\x1b[0m",
  "bright": "\x1b[1m",
  "dim": "\x1b[2m",
  "underscore": "\x1b[4m",
  "blink": "\x1b[5m",
  "reverse": "\x1b[7m",
  "hidden": "\x1b[8m",
  "black": "\x1b[30m",
  "bgBlack": "\x1b[40m",
  "bgRed": "\x1b[41m",
  "bgGreen": "\x1b[42m",
  "bgYellow": "\x1b[43m",
  "bgBlue": "\x1b[44m",
  "bgMagenta": "\x1b[45m",
  "bgCyan": "\x1b[46m",
  "bgWhite": "\x1b[47m",
};

class Console {
  void log(Object? obj, String? msg, {String color = "cyan"}) {
    var colorCode = debugColors[color];
    printDebug(
        "$colorCode◢◤◢◤◢◤◢◤◢◤◢◤◢◤◢◤◢◤◢◤◢◤◢◤◢◤ ${msg ?? 'DEBUG'} ◢◤◢◤◢◤◢◤◢◤◢◤◢◤◢◤◢◤◢◤◢◤◢◤◢◤${debugColors['end']}");
    if (obj is Map) {
      printDebug(
          "${debugColors['white']}${debugEncoder.convert(obj)}${debugColors['end']}");
    } else if (obj != null) {
      printDebug(obj);
    }
  }

  void printDebug(Object obj) {
    print(obj);
  }

  void trace(dynamic Function() fn) {
    Chain.capture(fn);
  }

  Future<void> initDebug(Function callback, String name) async {
    try {
      log(callback, "${name}_INIT_START");
      var result = await callback();
      log(result, "${name}_INIT_SUCCESS");
    } catch (e) {
      log(e, "${name}_INIT_ERROR");
    }
  }
}

var console = Console();
