String dateFormat(
  DateTime date, {
  bool? time = false,
  String? template = "", // TODO templating
}) {
  var dateString =
      "${date.day.toString().padLeft(2, '0')}-${date.month.toString().padLeft(2, '0')}-${date.year.toString()}";
  if (time == true) {
    dateString += " ${date.hour.toString()}:${date.minute.toString()}";
  }
  return dateString;
}
