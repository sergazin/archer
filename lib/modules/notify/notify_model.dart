import 'package:equatable/equatable.dart';

enum NotifyTypes { warning, error, success, info }

enum NotifyKind { alert, snackbar }

class Notify extends Equatable {
  final String? title;
  final String message;
  final NotifyTypes type;
  final NotifyKind kind;
  const Notify(
      {this.message = "Notify Message",
      this.type = NotifyTypes.info,
      this.kind = NotifyKind.alert,
      this.title});

  @override
  List<Object?> get props => [title, message, type, kind];
}
