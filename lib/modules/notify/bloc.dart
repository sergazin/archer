import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'notify_model.dart';

const emptyNotify = Notify(message: '');

// EVENTS ===============================================================
class NotifyEvent {
  const NotifyEvent();
}

class HideNotifyEvent extends NotifyEvent {
  const HideNotifyEvent();
}

class ShowNotifyEvent extends NotifyEvent {
  final Notify notify;
  const ShowNotifyEvent(this.notify);
}

// STATE ===============================================================
enum NotifyStatus { shown, hidden }


class NotifyState extends Equatable {
  final NotifyStatus status;
  final Notify notify;

  const NotifyState._({
    this.notify = emptyNotify,
    this.status = NotifyStatus.hidden,
  });

  @override
  List<Object?> get props => [status];

  // STATES
  const NotifyState.hidden() : this._();

  const NotifyState.shown(Notify notify, {NotifyKind? kind})
      : this._(
          status: NotifyStatus.shown,
          notify: notify,
        );
}

// BLOCk ===================================
class NotifyBloc extends Bloc<NotifyEvent, NotifyState> {
  NotifyBloc() : super(const NotifyState.hidden()) {
    on<ShowNotifyEvent>((event, emit) async {
      emit(NotifyState.shown(event.notify));
    });
    on<HideNotifyEvent>((event, emit) async {
      emit(NotifyState.hidden());
    });
  }
}
