import 'dart:math';
import 'package:equatable/equatable.dart';
import 'package:latlong2/latlong.dart';

class Geo extends Equatable {
  final double x;
  final double y;

  const Geo(this.x, this.y);

  @override
  List<Object?> get props => [x, y];

  @override
  bool? get stringify => true;

  Geo operator +(Geo other) {
    return Geo(x + other.x, y + other.y);
  }

  Geo operator *(double other) {
    return Geo(x * other, y * other);
  }

  LatLng toLatLng() {
    return LatLng(x, y);
  }

  bool get isEmpty {
    return this == Geo.empty;
  }

  Point toPoint() {
    return Point<double>(x, y);
  }

  // From JSON ============================================================
  factory Geo.fromJson(List<dynamic>? json) {
    if (json == null) return Geo.empty;
    double x = json.first != null
        ? (json[0] is double
            ? json[0] as double
            : json[0] is int
                ? json[0]!.toDouble()
                : 0.0)
        : 0.0;
    double y = json.last != null
        ? (json[1] is double
            ? json[1] as double
            : json[1] is int
                ? json[1]!.toDouble()
                : 0.0)
        : 0.0;
    return Geo(x, y);
  }

  static const Geo empty = Geo(0, 0);
}
