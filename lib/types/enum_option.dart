// (c) 2022 | Sergazin Software Dart Blueprint

import 'package:equatable/equatable.dart';

class EnumOption extends Equatable {
  // Props ================================================================
  final String l;
  final dynamic v;
  // Constructor ==========================================================
  const EnumOption({
    required this.l,
    required this.v,
  });
  // From JSON ============================================================
  factory EnumOption.fromJson(Map<String, dynamic> json) {
    return EnumOption(
      l: json['l'],
      v: json['v'] ?? '',
    );
  }
  // To JSON ==============================================================
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['l'] = l;
    data['v'] = v;
    return data;
  }

  // Equatable Props =====================================================
  @override
  List<Object> get props => [];
}
