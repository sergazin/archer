// Motivation: Standart DateTime not created as CONST

import 'package:equatable/equatable.dart';
import '../tools/date_format.dart';

class LazyDate extends Equatable {
  final String? _value; // as iso string

  const LazyDate(this._value);

  factory LazyDate.fromDate(DateTime date) {
    return LazyDate(date.toUtc().toIso8601String());
  }

  DateTime get value {
    if (_value == null) return DateTime.now();
    var result = DateTime.tryParse(_value!);
    if (result == null) return DateTime.now();
    return result;
  }

  String get iso {
    return value.toIso8601String();
  }

  String format({String? template, bool? time}) {
    //if (value == null) return "Дата не указана";
    return dateFormat(value, template: template, time: time);
  }

  bool get isEmpty {
    return _value == null;
  }

  @override
  toString() {
    return _value ?? '-';
  }

  toJson() {
    return _value ?? '1970-01-01T00:00:00.000Z';
  }

  static const empty = LazyDate(null);

  @override
  List<Object?> get props => [_value];
}
